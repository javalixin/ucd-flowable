package com.ucd.web.controller.system;

import com.ucd.common.constant.Constants;
import com.ucd.common.core.domain.AjaxResult;
import com.ucd.common.core.domain.R;
import com.ucd.common.core.domain.entity.SysMenu;
import com.ucd.common.core.domain.entity.SysUser;
import com.ucd.common.core.domain.model.LoginBody;
import com.ucd.common.core.domain.model.LoginUser;
import com.ucd.common.utils.RSAUtils;
import com.ucd.common.utils.ServletUtils;
import com.ucd.framework.web.service.SysLoginService;
import com.ucd.framework.web.service.SysPermissionService;
import com.ucd.framework.web.service.TokenService;
import com.ucd.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * 登录验证
 *
 * @author ucd
 */
@RestController
public class SysLoginController {
	@Autowired
	private SysLoginService loginService;

	@Autowired
	private ISysMenuService menuService;

	@Autowired
	private SysPermissionService permissionService;

	@Autowired
	private TokenService tokenService;

	/**
	 * 登录方法
	 *
	 * @param loginBody 登录信息
	 * @return 结果
	 */
	@PostMapping("/login")
	public R login(@RequestBody LoginBody loginBody) throws Exception {
		try {
			String username = loginBody.getUsername();
			String password = loginBody.getPassword();
			String code = loginBody.getCode();
			String uuid = loginBody.getUuid();
			password = RSAUtils.decryptByPrivateKey(password);
			String token = loginService.login(username, password, code, uuid);
			ServletUtils.getResponse().setHeader(Constants.TOKEN, token);
			return R.ok();
		} catch (Exception e) {
			e.printStackTrace();
			return R.failed(e.getMessage());
		}
	}

	/**
	 * 获取用户信息
	 *
	 * @return 用户信息
	 */
	@GetMapping("getInfo")
	public AjaxResult getInfo() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
		SysUser user = loginUser.getUser();
		// 角色集合
		Set<String> roles = permissionService.getRolePermission(user);
		// 权限集合
		Set<String> permissions = permissionService.getMenuPermission(user);
		AjaxResult ajax = AjaxResult.success();
		ajax.put("user", user);
		ajax.put("roles", roles);
		ajax.put("permissions", permissions);
		return ajax;
	}

	/**
	 * 获取路由信息
	 *
	 * @return 路由信息
	 */
	@GetMapping("getRouters")
	public AjaxResult getRouters() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
		// 用户信息
		SysUser user = loginUser.getUser();
		List<SysMenu> menus = menuService.selectMenuTreeByUserId(user.getUserId());
		return AjaxResult.success(menuService.buildMenus(menus));
	}
}
