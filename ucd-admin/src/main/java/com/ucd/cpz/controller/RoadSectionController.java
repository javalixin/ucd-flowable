package com.ucd.cpz.controller;

import com.ucd.common.annotation.Log;
import com.ucd.common.core.controller.BaseController;
import com.ucd.common.core.domain.AjaxResult;
import com.ucd.common.core.domain.R;
import com.ucd.common.core.page.TableDataInfo;
import com.ucd.common.enums.BusinessType;
import com.ucd.common.utils.poi.ExcelUtil;
import com.ucd.cpz.domain.dto.RoadSectionDto;
import com.ucd.cpz.domain.vo.RoadSectionVo;
import com.ucd.cpz.service.IRoadSectionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 巡查路段Controller
 *
 * @author ucd
 * @date 2021-05-18
 */
@RestController
@RequestMapping("/cpz/section")
public class RoadSectionController extends BaseController {
    @Autowired
    private IRoadSectionService roadSectionService;

    /**
     * 查询巡查路段列表
     */
    @ApiOperation(value = "查询巡查路段列表", response = R.class)
    @PreAuthorize("@ss.hasPermi('cpz:section:list')")
    @GetMapping("/list")
    public TableDataInfo list(RoadSectionDto roadSection) {
        startPage();
        List<RoadSectionVo> list = roadSectionService.selectRoadSectionList(roadSection);
        return getDataTable(list);
    }

    /**
     * 导出巡查路段列表
     */
    @ApiOperation(value = "导出巡查路段列表")
    @PreAuthorize("@ss.hasPermi('cpz:section:export')")
    @Log(title = "巡查路段", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(RoadSectionDto roadSection) {
        List<RoadSectionVo> list = roadSectionService.selectRoadSectionList(roadSection);
        ExcelUtil<RoadSectionVo> util = new ExcelUtil<RoadSectionVo>(RoadSectionVo.class);
        return util.exportExcel(list, "section");
    }

    /**
     * 获取巡查路段详细信息
     */
    @ApiOperation(value = "获取巡查路段详细信息")
    @PreAuthorize("@ss.hasPermi('cpz:section:query')")
    @GetMapping(value = "/{id}")
    public R<RoadSectionVo> getInfo(@PathVariable("id") Long id) {
        return R.ok(roadSectionService.selectRoadSectionById(id));
    }

    /**
     * 新增巡查路段
     */
    @ApiOperation(value = "新增巡查路段", response = R.class)
    @PreAuthorize("@ss.hasPermi('cpz:section:add')")
    @Log(title = "巡查路段", businessType = BusinessType.INSERT)
    @PostMapping
    public R add(@RequestBody RoadSectionDto roadSection) {
        roadSectionService.insertRoadSection(roadSection);
        return R.ok();
    }

    /**
     * 修改巡查路段
     */
    @ApiOperation(value = "修改巡查路段", response = R.class)
    @PreAuthorize("@ss.hasPermi('cpz:section:edit')")
    @Log(title = "巡查路段", businessType = BusinessType.UPDATE)
    @PutMapping
    public R edit(@RequestBody RoadSectionDto roadSection) {
        roadSectionService.updateRoadSection(roadSection);
        return R.ok();
    }

    /**
     * 删除巡查路段
     */
    @ApiOperation(value = "删除巡查路段", response = R.class)
    @PreAuthorize("@ss.hasPermi('cpz:section:remove')")
    @Log(title = "巡查路段", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public R remove(@PathVariable Long[] ids) {
        roadSectionService.deleteRoadSectionByIds(ids);
        return R.ok();
    }
}
