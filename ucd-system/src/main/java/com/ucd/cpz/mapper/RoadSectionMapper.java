package com.ucd.cpz.mapper;

import com.ucd.cpz.domain.dto.RoadSectionDto;
import com.ucd.cpz.domain.vo.RoadSectionVo;

import java.util.List;

/**
 * 巡查路段Mapper接口
 *
 * @author ucd
 * @date 2021-05-18
 */
public interface RoadSectionMapper {
    /**
     * 查询巡查路段
     *
     * @param id 巡查路段ID
     * @return 巡查路段
     */
    public RoadSectionVo selectRoadSectionById(Long id);

    /**
     * 查询巡查路段列表
     *
     * @param roadSection 巡查路段
     * @return 巡查路段集合
     */
    public List<RoadSectionVo> selectRoadSectionList(RoadSectionDto roadSection);

    /**
     * 新增巡查路段
     *
     * @param roadSection 巡查路段
     * @return 结果
     */
    public int insertRoadSection(RoadSectionDto roadSection);

    /**
     * 修改巡查路段
     *
     * @param roadSection 巡查路段
     * @return 结果
     */
    public int updateRoadSection(RoadSectionDto roadSection);

    /**
     * 删除巡查路段
     *
     * @param id 巡查路段ID
     * @return 结果
     */
    public int deleteRoadSectionById(Long id);

    /**
     * 批量删除巡查路段
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoadSectionByIds(Long[] ids);
}
