package com.ucd.cpz.service.impl;

import com.ucd.cpz.domain.dto.RoadSectionDto;
import com.ucd.cpz.domain.vo.RoadSectionVo;
import com.ucd.cpz.mapper.RoadSectionMapper;
import com.ucd.cpz.service.IRoadSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 巡查路段Service业务层处理
 *
 * @author ucd
 * @date 2021-05-18
 */
@Service
public class RoadSectionServiceImpl implements IRoadSectionService {
    @Autowired
    private RoadSectionMapper roadSectionMapper;

    /**
     * 查询巡查路段
     *
     * @param id 巡查路段ID
     * @return 巡查路段
     */
    @Override
    public RoadSectionVo selectRoadSectionById(Long id) {
        return roadSectionMapper.selectRoadSectionById(id);
    }

    /**
     * 查询巡查路段列表
     *
     * @param roadSection 巡查路段
     * @return 巡查路段
     */
    @Override
    public List<RoadSectionVo> selectRoadSectionList(RoadSectionDto roadSection) {
        return roadSectionMapper.selectRoadSectionList(roadSection);
    }

    /**
     * 新增巡查路段
     *
     * @param roadSection 巡查路段
     * @return 结果
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public int insertRoadSection(RoadSectionDto roadSection) {
        return roadSectionMapper.insertRoadSection(roadSection);
    }

    /**
     * 修改巡查路段
     *
     * @param roadSection 巡查路段
     * @return 结果
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public int updateRoadSection(RoadSectionDto roadSection) {
        return roadSectionMapper.updateRoadSection(roadSection);
    }

    /**
     * 批量删除巡查路段
     *
     * @param ids 需要删除的巡查路段ID
     * @return 结果
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public int deleteRoadSectionByIds(Long[] ids) {
        return roadSectionMapper.deleteRoadSectionByIds(ids);
    }

    /**
     * 删除巡查路段信息
     *
     * @param id 巡查路段ID
     * @return 结果
     */
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public int deleteRoadSectionById(Long id) {
        return roadSectionMapper.deleteRoadSectionById(id);
    }
}
