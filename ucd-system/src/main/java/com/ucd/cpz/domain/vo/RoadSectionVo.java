package com.ucd.cpz.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.ucd.common.annotation.Excel;

/**
 * 巡查路段对象 cpz_road_section
 *
 * @author ucd
 * @date 2021-05-18
 */
@Data
@ApiModel(value = "巡查路段 VO")
public class RoadSectionVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** ID */
    @ApiModelProperty(value = "ID")
    private Long id;
    /** 线路 */
    @Excel(name = "线路")
    @ApiModelProperty(value = "线路")
    private Long lineId;
    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;
    /** 路段名称 */
    @Excel(name = "路段名称")
    @ApiModelProperty(value = "路段名称")
    private String sectionName;
    /** 起始站点 */
    @Excel(name = "起始站点")
    @ApiModelProperty(value = "起始站点")
    private Long startId;
    /** 结束站点 */
    @Excel(name = "结束站点")
    @ApiModelProperty(value = "结束站点")
    private Long endId;
    /** 风险等级 */
    @Excel(name = "风险等级")
    @ApiModelProperty(value = "风险等级")
    private String riskLevel;
    /** 巡查频次 */
    @Excel(name = "巡查频次")
    @ApiModelProperty(value = "巡查频次")
    private BigDecimal frequency;
    /** 隧道里程 */
    @Excel(name = "隧道里程")
    @ApiModelProperty(value = "隧道里程")
    private BigDecimal tunnel;
    /** 高架里程 */
    @Excel(name = "高架里程")
    @ApiModelProperty(value = "高架里程")
    private BigDecimal viaduct;
    /** 排序 */
    @Excel(name = "排序")
    @ApiModelProperty(value = "排序")
    private Integer sort;
    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String status;
    /** 备注 */
    @ApiModelProperty(value = "备注")
    private String remark;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createBy;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateBy;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

}
