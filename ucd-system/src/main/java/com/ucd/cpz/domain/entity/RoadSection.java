package com.ucd.cpz.domain.entity;

import java.math.BigDecimal;
import com.ucd.common.core.domain.BaseEntity;

/**
 * 巡查路段对象 cpz_road_section
 *
 * @author ucd
 * @date 2021-05-18
 */
public class RoadSection extends BaseEntity{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 线路 */
    private Long lineId;
    /** 类型 */
    private String type;
    /** 路段名称 */
    private String sectionName;
    /** 起始站点 */
    private Long startId;
    /** 结束站点 */
    private Long endId;
    /** 风险等级 */
    private String riskLevel;
    /** 巡查频次 */
    private BigDecimal frequency;
    /** 隧道里程 */
    private BigDecimal tunnel;
    /** 高架里程 */
    private BigDecimal viaduct;
    /** 排序 */
    private Integer sort;
    /** 状态 */
    private String status;
}
