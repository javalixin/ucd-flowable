## 项目说明

基RuoYi-vue 4.x + flowable 6.5 的工作流管理 

源项目地址：https://gitee.com/tony2y/RuoYi-flowable

Flowable相关文档 [flowable初级使用手册](https://www.shangmayuan.com/a/538a0b230dbe4798b273305b.html)


## 演示地址

源项目演示地址（主要关注工作流功能）：http://129.211.46.183/

**为了方便体验，请勿删除和改动初始化的几个流程和表单，感谢！**

## 项目地址

Gitee：https://gitee.com/javalixin/ucd-flowable.git **个人仓库（留待后续持续优化升级）**

## 技术方案
* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 特别鸣谢：
 [RuoYi](https://gitee.com/y_project/RuoYi-Vue) | [流程设计器](https://github.com/GoldSubmarine/workflow-bpmn-modeler) | [表单设计器](https://github.com/JakHuang/form-generator) 
 
## 内置功能
* 流程设计
* 表单配置
* 流程发起
* 流转处理
## 参考文档

文档地址：http://doc.ruoyi.vip/ruoyi

## 代码生成器
### 工具使用说明
> 生成controller、service、dao、entity、xml、static

例如：创建一个cms 文章表 cms_article cms为模块名 article为业务名
1. 主键建议为id 不推荐article_id
2. 数据库字段命名规范 article_name 不要写成 articleName
3. 状态类型等字段建议0表示否 1表示是,DB类型建议为char或varchar
4. 不要用state 而用status 原因:前端框架可能有关键字冲突 如：field: 'state'
5. 表注释将作为菜单名称、接口注释、前端列表名称等
6. 表字段注释中$前的内容将作为实体属性、接口注释、前端表单属性名称等
6. 表字段注释中可加入$xxx$标签辅助生成代码
* 表字段注释标签说明如下：
    * ```$search$``` ：可以自动生成查询条件，默认为匹配查询
    * ```$EQ$``` ：配合search标签使用，查询类型为匹配查询
    * ```$LIKE$``` ：配合search标签使用，查询类型为模糊查询
    * ```$BETWEEN$``` ：配合search标签使用，查询类型为区间查询
    * ```$notlist$``` ：不在列表中显示该字段
    * ```$notadd$``` ：不在新增表单中显示该字段
    * ```$notedit$``` ：不在修改表单中显示该字段
    * ```$text$``` ：文本域
    * ```$image$``` ：图片上传
    * ```$file$``` ：文件上传
    * ```$editor$``` ：富文编辑器
    * ```$switch***switch$``` ：switch 如：```$switch[{"key":0,"value":"否"},{"key":1,"value":"是"}]switch$```
    * ```$select***select$``` ：select 如：```$select[{"key":0,"value":"进行中"},{"key":1,"value":"已完成"}]select$```
    * ```$radio***radio$``` ：radio 如：```$radio[{"key":0,"value":"隐藏"},{"key":1,"value":"显示"}]radio$```
    * ```$checkbox***checkbox$``` ：checkbox 如：```$checkbox[{"key":0,"value":"读书"},{"key":1,"value":"画画"}]checkbox$```
    * ```$dict[xxx]dict$``` ：字典数据源，配合switch、select、radio、checkbox标签使用，如：```$select[]select$$dict[risk_level]dict$```
    * 如果只需要空的switch、select、radio、checkbox组件，数据选项自行编写获取，则只需传入空数据即可，如：```$select[]select$```
---
### 项目生成示例
> 表设计SQL
```sql
CREATE TABLE `cpz_road_section` (
`id` BIGINT ( 20 ) NOT NULL AUTO_INCREMENT COMMENT 'ID',
`line_id` BIGINT ( 20 ) NOT NULL COMMENT '线路$search$$select[]select$',
`type` CHAR ( 1 ) COLLATE utf8mb4_bin DEFAULT '1' COMMENT '类型$search$$select[{"key":1,"value":"线路路段"},{"key":2,"value":"外线路"}]select$',
`section_name` VARCHAR ( 30 ) COLLATE utf8mb4_bin NOT NULL COMMENT '路段名称',
`start_id` BIGINT ( 20 ) DEFAULT NULL COMMENT '起始站点$select[]select$',
`end_id` BIGINT ( 20 ) DEFAULT NULL COMMENT '结束站点$select[]select$',
`risk_level` CHAR ( 1 ) COLLATE utf8mb4_bin NOT NULL COMMENT '风险等级$search$$select[]select$$dict[risk_level]dict$',
`frequency` DECIMAL ( 10, 2 ) NOT NULL COMMENT '巡查频次$search$$select[]select$$dict[frequency]dict$',
`tunnel` DECIMAL ( 10, 2 ) DEFAULT '0.00' COMMENT '隧道里程',
`viaduct` DECIMAL ( 10, 2 ) DEFAULT '0.00' COMMENT '高架里程',
`sort` INT ( 10 ) DEFAULT '0' COMMENT '排序',
`status` CHAR ( 1 ) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '状态$search$$switch[{"key":0,"value":"停用"},{"key":1,"value":"启用"}]switch$',
`remark` VARCHAR ( 225 ) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注$notlist$',
`create_by` VARCHAR ( 64 ) COLLATE utf8mb4_bin DEFAULT '' COMMENT '创建者',
`create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`update_by` VARCHAR ( 64 ) COLLATE utf8mb4_bin DEFAULT '' COMMENT '更新者',
`update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY ( `id` ) USING BTREE
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '巡查路段';
```
---

> 生成文件结构图 

![文件图](https://images.gitee.com/uploads/images/2021/0518/180134_b795aecc_623606.png "代码生成文件图")

---
> 代码生成演示图 

![演示图](https://images.gitee.com/uploads/images/2021/0518/174328_113778b7_623606.png "代码生成演示图")

---
> 代码生成效果图 

![效果图](https://images.gitee.com/uploads/images/2021/0518/180419_e1899bf7_623606.png "代码生成效果图")



