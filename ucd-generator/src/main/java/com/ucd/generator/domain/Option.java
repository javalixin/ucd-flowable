package com.ucd.generator.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 选项实体
 *
 * @ClassName: Option
 * @Description:
 * @Date: 2021/5/12 15:59
 * @Author: javalx
 * @Version: 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Option implements Serializable {
    private String key = "";
    private String value = "";
}
