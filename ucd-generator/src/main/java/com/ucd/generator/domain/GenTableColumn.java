package com.ucd.generator.domain;

import javax.validation.constraints.NotBlank;

import com.ucd.common.core.domain.BaseEntity;
import com.ucd.common.utils.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 代码生成业务字段表 gen_table_column
 *
 * @author ucd
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenTableColumn extends BaseEntity {
    private static final long serialVersionUID = 7699097534609781478L;
    /**
     * 编号
     */
    private Long columnId;

    /**
     * 归属表编号
     */
    private Long tableId;

    /**
     * 列名称
     */
    private String columnName;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 列类型
     */
    private String columnType;

    /**
     * JAVA类型
     */
    private String javaType;

    /**
     * JAVA字段名
     */
    @NotBlank(message = "Java属性不能为空")
    private String javaField;

    /**
     * 是否主键（1是）
     */
    private String isPk;

    /**
     * 是否自增（1是）
     */
    private String isIncrement;

    /**
     * 是否必填（1是）
     */
    private String isRequired;

    /**
     * 是否为插入字段（1是）
     */
    private String isInsert;

    /**
     * 是否编辑字段（1是）
     */
    private String isEdit;

    /**
     * 是否列表字段（1是）
     */
    private String isList;

    /**
     * 是否查询字段（1是）
     */
    private String isQuery;

    /**
     * 查询方式（EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围）
     */
    private String queryType;

    /**
     * 显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件、image图片上传控件、upload文件上传控件、editor富文本控件）
     */
    private String htmlType;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 下拉框|radio|check值
     */
    private List<Option> options = null;

    /**
     * 下拉框|radio|check选项json
     */
    private String optionStr;

    /**
     * 是否主键
     *
     * @return
     */
    public boolean isPk() {
        return this.isPk != null && StringUtils.equals("1", this.isPk);
    }

    /**
     * 是否父类字段
     *
     * @return
     */
    public boolean isSuperColumn() {
        return isSuperColumn(this.javaField);
    }

    public static boolean isSuperColumn(String javaField) {
        return StringUtils.equalsAnyIgnoreCase(javaField,
                // BaseEntity
                "createBy", "createTime", "updateBy", "updateTime", "remark",
                // TreeEntity
                "parentName", "parentId", "orderNum", "ancestors");
    }

    /**
     * 是否列表字段(显示与否)
     *
     * @return
     */
    public boolean isList() {
        return this.isList != null && StringUtils.equals("1", this.isList);
    }
}
