package com.ucd.common.exception.user;

import com.ucd.common.exception.BaseException;

/**
 * 用户信息异常类
 * 
 * @author ucd
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
