/*
 * Copyright (c) 2021-2099 http://o.bjucd.com
 *
 */

package com.ucd.common.core.domain;

import com.ucd.common.constant.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @param <T>
 * @author javalx
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "响应信息主体")
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "返回标记：成功标记=200，失败标记=500，……")
    private int code;

    @ApiModelProperty(value = "返回信息")
    private String msg;


    @ApiModelProperty(value = "数据")
    private T data;

    public static <T> R<T> ok() {
        return restResult(null, HttpStatus.SUCCESS, null);
    }

    public static <T> R<T> ok(T data) {
        return restResult(data, HttpStatus.SUCCESS, null);
    }

    public static <T> R<T> ok(T data, String msg) {
        return restResult(data, HttpStatus.SUCCESS, msg);
    }

    public static <T> R<T> failed() {
        return restResult(null, HttpStatus.ERROR, null);
    }

    public static <T> R<T> failed(String msg) {
        return restResult(null, HttpStatus.ERROR, msg);
    }

    public static <T> R<T> failed(T data) {
        return restResult(data, HttpStatus.ERROR, null);
    }

    public static <T> R<T> failed(T data, String msg) {
        return restResult(data, HttpStatus.ERROR, msg);
    }

    private static <T> R<T> restResult(T data, int code, String msg) {
        R<T> apiResult = new R<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }
}
