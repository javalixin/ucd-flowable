package com.ucd.common.enums;

/**
 * 操作状态
 *
 * @author ucd
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
