package com.ucd.common.enums;

/**
 * 数据源
 *
 * @author ucd
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
